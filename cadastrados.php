<?php 

require_once 'conexao/conecta.php';
session_start();


if (!isset($_SESSION['logado'])) :
	header('Location: index.php');
endif;



 ?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  </head>
  <body>
   

<div class="container">
    <div class="row justify-content-center">
     <div class=" table-responsive"> 
			<h2 class="text-center mt-5">Usuários cadastrados</h2>
			<?php 
					
					if(isset($_SESSION['up'])){
						echo $_SESSION['up'];
						unset($_SESSION['up']);
						}
					

	?>
    
               <table class="table table-striped table-bordered table-hover">
			   	<thead>
				   <tr>
				   <td>Id</td>
				   <td>Nome</td>
				   <td>Username</td>
				   <td>E-mail</td>
				   <td>Ações</td>
				
				   </tr>
				</thead>
				<tbody>
				<?php
				$sql = "SELECT * FROM USUARIO";
				$resultado = mysqli_query($conn,$sql);
				while($dados = mysqli_fetch_array($resultado)):
				?>
				<tr >
				   <td ><?php echo $dados['id']; ?></td>
				   <td ><?php echo $dados['nome']; ?></td>
				   <td ><?php echo $dados['username']; ?></td>
				   <td ><?php echo $dados['email']; ?></td>
				   <td>
				   <a href="editar/editar_usu.php?id=<?php echo $dados['id'];?>" class="btn btn-warning">Editar</a>
				   <a href="deletar/deletar_usu.php?id=<?php echo $dados['id'];?>" class="btn btn-danger">Deletar</a>
				   </td>
				   

				 
				</tr>
				<?php
				endwhile;
				?>
				</tbody>
			   </table>
</div> 
	</div>
		</div>


		<div class="container">
    <div class="row justify-content-center">
      <div class="table-responsive">
			<h2 class="text-center mt-2">Alunos cadastrados</h2>
               <table class="table table-striped table-bordered table-hover">
			   	<thead >
				   <tr class="">
				   <td>Id</td>
				   <td >Nome</td>
				   <td >Data de nascimento</td>	
				   <td>Ações</td>		
				   </tr>
				</thead>
				<tbody>
				<?php
				$sql = "SELECT * FROM Aluno";
				$resultado = mysqli_query($conn,$sql);
				while($dados = mysqli_fetch_array($resultado)):
				?>
				<tr >
				   <td ><?php echo $dados['id']; ?></td>
				   <td ><?php echo $dados['nome']; ?></td>
				   <td ><?php echo $dados['nasc']; ?></td>
				   <td>
				   <a href="editar/editar_alu.php?id=<?php echo $dados['id'];?>" class="btn btn-warning">Editar</a>
				   <a href="deletar/deletar_alu.php?id=<?php echo $dados['id'];?>" class="btn btn-danger" name="btn-deletar">Deletar</a>
				   </td>
				</tr>
				<?php
				endwhile;
				?>
				</tbody>
			   </table>
</div> 
	</div>
		</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>