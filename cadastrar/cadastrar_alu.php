<!DOCTYPE html>
<html lang="pt-br">
<head>
 <!-- Required meta tags -->
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
	<body>



<br><br>
		<h1 class="text-center mt-5">Cadastrar Aluno</h1>


		<div class="container">

		<div class="row justify-content-center">
		<div class="col-lg-6 col-md-6 col-sm-8">


		<form method="POST" action="processa_alu.php" >
			



		  <div class="form-group">

		  	<div  class="input-group">
			
				<input  class="form-control" type="text" name="nome" placeholder="Digite seu nome completo">
			</div>
			</div>
		
		  <div class="form-group">	
		
              <label for="data">Data de nascimento</label>
              <input type="date" class="form-control" id="data" name="data" >        
			
		  </div>


			<button type="submit" class="btn btn-primary">Cadastrar</button>
			<a href="index.php" class="btn btn-link">Login</a>
			
			
			
			
			</form>
			
			</div>
			</div>

		

</div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>