<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Sistema Académico</title>

  </head>
  <body>
    <br><br>
    <h1 class="text-center mt-5">Sistema Académico</h1>




<div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6 col-sm-8">
                <form action="login.php	" method="POST">

				<?php 
					session_start();
					if(isset($_SESSION['msg'])){
						echo $_SESSION['msg'];
						unset($_SESSION['msg']);
						}
					

	?>
    
    <div class="form-group mt-3">
        <label for="username">Username</label>
            <input type="text" class="form-control " id="username" name="username" placeholder="Digite seu username	" required>
            </div>


  <div class="form-group">
        <label for="senha">Senha</label>
        <input type="password" class="form-control " id="senha" name="senha" placeholder="Digite sua senha" required>
  </div>
  
  <button type="submit" class="btn btn-primary" name="btn-entrar">Logar</button>
  <a href="cadastrar/escolher.php">Cadastrar</a>
</form>


  
</div>
</div>
</div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>